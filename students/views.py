from django.shortcuts import render
from django.views.generic import ListView 
from  .models import Student 
from django_xhtml2pdf.views import PdfMixin
from django.http import JsonResponse, HttpResponse
import xlwt
import csv

def export_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="students.csv"'

    writer = csv.writer(response)
    writer.writerow(['Admission', 'Surname', 'Name'])

    students = Student.objects.all().values_list('admission', 'last_name', 'first_name')
    for student in students:
        writer.writerow(student)

    return response

def export_students(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="students.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Students')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Admission', 'Surname ', 'Name' , ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    rows = Student.objects.all().values_list('admission', 'last_name', 'first_name')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response

class StudentListView(ListView):
	model =Student
	template_name ='student_list.html'


class StudentPdfView(PdfMixin,ListView):
    model = Student
    template_name = "student_pdf.html"



def get_students(request):
    students = Student.objects.all().values() # or simply .values() to get all fields
    students_list = list(students)  # important: convert the QuerySet to a list object
    return JsonResponse(students_list, safe=False)