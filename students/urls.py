from django.urls import path
from  .views import StudentListView, StudentPdfView, get_students, export_students, export_csv

urlpatterns =[        
          path("", StudentListView.as_view(),name="students"),
          path("pdf", StudentPdfView.as_view(),name="pdf"),
          path ("json", get_students, name='json'), 
          path ("excel", export_students, name='excel'), 
          path ("csv", export_csv, name='csv'), 
          
] 

