from django.db import models

# Create your models here.
class Student(models.Model):
	admission =models.PositiveIntegerField() 
	last_name =models.CharField (max_length =10)
	first_name =models.CharField (max_length =10)
	
	def __str__(self):
		return self.last_name
	class Meta:
		ordering =('admission', ) 
	